<?php
/**
 * Codefathers Magento Image Module
 *
 * @category    Cf
 * @package     Cf_Image
 * @copyright   Copyright (c) Achim Schweisgut, codefathers 2016
 */

/**
 * Class Cf_Image_Model_Category
 */
class Cf_Image_Model_Category extends Cf_Image_Model_Media
{

    /**
     * @return string
     */
    public function getPathPrefix()
    {
        return 'catalog/category';
    }
    
}