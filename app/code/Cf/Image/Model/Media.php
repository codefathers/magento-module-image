<?php
/**
 * Codefathers Magento Image Module
 *
 * @category    Cf
 * @package     Cf_Image
 * @copyright   Copyright (c) Achim Schweisgut, codefathers 2016
 */
/**
 * Class Cf_Image_Model_Media
 */
class Cf_Image_Model_Media extends Cf_Image_Model_Abstract
{


    /**
     * returns the absolute file path + name 
     * @return string
     * 
     */
    public function getFile()
    {
        return $this->getMediaDir($this->getPath());
    }


    /**
     * @return string
     */
    public function getPathPrefix()
    {
        return '';
    }
    
    
    /**
     *
     * @return string
     */
    protected function getMediaDir($path = '')
    {
        $path = ($path) ? $this->_removeLeadingSlashes($path) : $path;
        $path = ($path) ? $this->_removeTrailingSlashes($path) : $path;
        $result = $this->_removeTrailingSlashes(Mage::getBaseDir('media')) . '/' . $this->getPathPrefix();
        $result = ($path) ? "$result/$path" : $result;
        return $result;
    }


    /**
     *
     * @return string
     */
    protected function getMediaUrl($path = '')
    {
        $path = ($path) ? $this->_removeLeadingSlashes($path) : $path;
        $path = ($path) ? $this->_removeTrailingSlashes($path) : $path;
        $store = $this->getStore();
        $baseUrl = $store->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);
        $result = $this->_removeTrailingSlashes($baseUrl) . '/' . $this->getPathPrefix();
        $result = ($path) ? "$result/$path" : $result;
        return $result;
    }




    /**
     *
     * @param integer $width
     * @param integer $height
     * @return string
     * @throws Exception
     */
    protected function _getUrl($width, $height)
    {
        if (!$this->exists()) {
            $default = Mage::getModel('cf_image/default');
            return $default->getUrl($width, $height);
        }
        $srcUrl = $this->getMediaUrl($this->getPath());
        if (!$width && !$height) {
            return $srcUrl;
        }

        $srcFile = $this->getMediaDir($this->getPath());
        $parts = pathinfo($srcFile);
        $dirName = $parts['dirname'];
        $fileName = $parts['filename'];
        $ext = $parts['extension'];

        $dstFileName = "$fileName-{$width}x{$height}.{$ext}";
        $dstDir = "$dirName/_cache";
        $dstFile = "$dstDir/$dstFileName";

        $result = $this->getMediaUrl(dirname($this->getPath()) . "/_cache/$dstFileName");
        if (is_readable($dstFile) && is_readable($dstFile)) {
            return $result;
        }

        /* check dir, generate if exists */
        if (!is_dir($dstDir)) {
            mkdir($dstDir, 0777, true);
            if (!is_dir($dstDir)) {
                throw new Exception ("could not create image dir '$dstDir'");
            }
        }

        $image = Cf_Image_Model_Adapter_Abstract::factory();
        $image->loadFromFile($srcFile);
        $image->resizeCrop($width, $height);
        $image->saveToFile($dstFile);
        if (!is_readable($dstFile) || !is_readable($dstFile)) {
            throw new Exception ("Could not create image file '$dstFile'");
        }
        return $result;
        
    }
    
    
    
}