<?php
/**
 * Codefathers Magento Image Module
 *
 * @category    Cf
 * @package     Cf_Image
 * @copyright   Copyright (c) Achim Schweisgut, codefathers 2016
 */


/**
 * Class Cf_Image_Model_Adapter_Abstract
 */
abstract class Cf_Image_Model_Adapter_Abstract
{

    
    CONST DEFAULT_ADAPTER_TYPE = 'gd';
    
    /**
     * 
     * @param string $type
     * @return Cf_Image_Model_Adapter_Abstract
     */
    static function factory($type = self::DEFAULT_ADAPTER_TYPE)
    {
        $className = "Cf_Image_Model_Adapter_$type";
        $className = uc_words($className, '_');
        $result = new $className();
        return $result;
    }
    
    
    /**
     * 
     */
    public function __construct()
    {
        /* to override */
    }    
    
    /**
     * 
     */
    public function __destroy()
    {
        /* to override */
    }    
    
    
    /**
     * 
     * @param string $filename
     * @return integer
     */
    static function getImageWidth($filename)
    {
        $size = self::getImageSize($filename);
        $result = (integer) $size['width'];
        return $result; 
    }    
    
    /**
     * 
     * @param string $filename
     * @return integer
     */
    static function getImageHeight($filename)
    {
        $size = self::getImageSize($filename);
        $result = (integer) $size['height'];
        return $result; 
    }  
    
    /**
     * 
     * @param string $filename
     * @return array
     * @throws Exception
     */
    static function getImageSize($filename)
    {
        if (!$filename) {
            throw new Exception ("this action requires a valid filename");
        }
        if (!file_exists($filename) || !is_readable($filename)) {
            throw new Exception ("this is not a valid image file '$filename'");
        }
        $dims = @getimagesize($filename);
        if (!$dims || !is_array($dims) || !count($dims)) {
            throw new Exception ("this is not a valid image file '$filename'");
        }
        $result = array(
            'width' => (integer) $dims[0],
            'height' => (integer) $dims[1]
        );
        return $result; 
    }     


}