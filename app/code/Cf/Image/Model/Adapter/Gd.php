<?php
/**
 * Codefathers Magento Image Module
 *
 * @category    Cf
 * @package     Cf_Image
 * @copyright   Copyright (c) Achim Schweisgut, codefathers 2016
 */

/**
 * Class Cf_Image_Model_Adapter_Gd
 */
class Cf_Image_Model_Adapter_Gd extends Cf_Image_Model_Adapter_Abstract
{

    protected $_quality = 100;
    protected $_canvas = null;
    protected $_fileName = '';

    /**
     * 
     */
    public function __construct()
    {
        parent::__construct();
        $this->_quality = 100;
        $this->_canvas = null;
        $this->_fileName = '';
    }
    
    
    /**
     * 
     */
    public function __destroy()
    {
        parent::__destroy();
        $this->clear();
    }    
    
    /**
     * 
     */
    public function clear()
    {
        $this->releaseCanvas();
    }    
        
    
    /**
     * 
     * @return string
     */
    public function getFileName()
    {
        return $this->_fileName;
    }

    /**
     * 
     * @param string $fileName
     */
    public function setFileName($fileName)
    {
        $this->_fileName = $fileName;
    }

    
    /**
     * 
     * @return integer
     */
    public function getQuality()
    {
        return (integer) $this->_quality;
    }

    /**
     * 
     * @param integer $quality
     */
    public function setQuality($quality)
    {
        $this->_quality = (integer) $quality;
    }

    
    

    /**
     * 
     * @param string $fileName
     * @throws Exception
     */
    public function loadFromFile($fileName)
    {
        $this->setCanvas(null);
        $this->_fileName = '';
        if (strlen($fileName)) {
            $ext = strtolower(substr($fileName, strrpos($fileName, '.') + 1));
            if (!file_exists($fileName) || !is_readable($fileName)) {
                throw new Exception("could not read file '$fileName'");
            }
            switch ($ext) {
                case 'png' :
                    $image = imagecreatefrompng($fileName);
                    $this->setCanvas($image);
                    break;
                case 'gif' :
                    $image = imagecreatefromgif($fileName);
                    $this->setCanvas($image);
                    break;
                case 'jpg' :
                case 'jpeg' :
                    $image = imagecreatefromjpeg($fileName);
                    $this->setCanvas($image);
                    break;
            }
        }
    }

    
    /**
     * 
     * @param string $fileName
     * @throws Exception
     * 
     */
    public function saveToFile($fileName, $quality = null)
    {
        $this->_canvasRequired();
        $ext = strtolower(substr($fileName, strrpos($fileName, '.') + 1));
        switch ($ext) {
            case 'png' :
                imagepng($this->getCanvas(), $fileName, 4);
                break;
            case 'gif' :
                imagegif($this->getCanvas(), $fileName);
                break;
            case 'jpg' :
                $quality = ($quality) ? $quality : $this->getQuality();
                if (!$quality) {
                    $quality = 100;
                }
                imagejpeg($this->getCanvas(), $fileName, $quality);
                break;
            default: 
                throw new Exception ("unknown file type '$ext'");
        }
        $this->setFileName($fileName);
    }

    /**
     * 
     * @param integer $width
     * @param integer $height
     */
    public function createCanvas($width, $height)
    {
        $canvas = $this->_createCanvas($width, $height);
        $this->setCanvas($canvas);
        $this->setFileName('');
    }
    
    /**
     * 
     * 
     * creates a canvas and returns it
     * 
     * @param integer $width
     * @param integer $height
     * @return canvas resource
     */
    protected function _createCanvas($width, $height)
    {
        $result = imagecreatetruecolor($width, $height);
        return $result;
    }
        
    
    
    /**
     * 
     * @param Object Resource $image
     */
    protected function setCanvas($image = null)
    {
        if (isset($this->_canvas)) {
            imagedestroy($this->_canvas);
        }
        $this->_canvas = $image;
    }

    /**
     * 
     * @return Object Resource $image
     */
    public function getCanvas()
    {
        return $this->_canvas;
    }

    /**
     * releases the canvas, clears it
     */
    public function releaseCanvas()
    {
        $this->setCanvas(null);
    }

    
    /**
     * 
     * @return boolean
     * @throws Exception
     */
    protected function _canvasRequired()
    {
        if (!isset($this->_canvas)) {
            throw new Exception("This action requires a valid canvas");
        }
        return true;
    }

    /**
     * 
     * returns the width if my canvas
     * 
     * @return integer
     */
    public function getWidth()
    {
        return (isset($this->_canvas)) ? imagesx($this->_canvas) : 0;
    }


    /**
     * 
     * returns the height if my canvas
     * 
     * @return integer
     */
    public function getHeight()
    {
        return (isset($this->_canvas)) ? imagesy($this->_canvas) : 0;
    }

    

    /**
     * resize_crop
     *
     */
    public function resizeCrop($width = 0, $height = 0, $scale = 100)
    {
        $this->_canvasRequired();

        if (!$width && !$height) {
            throw new Exception("resizeCrop needs width or height value");
        } elseif ($width && !$height) {
            $height = round($this->getHeight() / $this->getWidth() * $width);
        } elseif (!$width && $height) {
            $width = round($this->getWidth() / $this->getHeight() * $height);
        }

        $scale = max(1, min($scale, 100));
        
        /*
          try to fit vertical
          assume that the height = imageheight
          and calculate the depending width
         */
        $cpHeight = $this->getHeight();
        $cpWidth = round($cpHeight / $height * $width);
        if ($cpWidth > $this->getWidth()) {
            /*
              that doesent fit, try to fit horizontal
              assume that the width = imagewidth
              and calculate the depending height
             */
            $cpWidth = $this->getWidth();
            $cpHeight = round($cpWidth / $width * $height);
        }

        if ($scale < 100) {
            $cpWidth = $cpWidth * $scale / 100;
            $cpHeight = $cpHeight * $scale / 100;
        }

        $cpLeft = round(($this->getWidth() / 2) - ($cpWidth / 2));
        $cpTop = round(($this->getHeight() / 2) - ($cpHeight / 2));

        $resizedImage = $this->_createCanvas($width, $height);
        $trColor = ImageColorAllocate($resizedImage, 0, 0, 0);
        ImageColorTransparent($resizedImage, $trColor);

        imagealphablending($resizedImage, false);
        $color = imagecolorallocatealpha($resizedImage, 0, 0, 0, 127);
        imagefill($resizedImage, 0, 0, $color);
        imagesavealpha($resizedImage, true);


        imagecopyresampled(
                $resizedImage, $this->getCanvas(), 0, 0, $cpLeft, $cpTop, $width, $height, $cpWidth, $cpHeight
        );
        $this->setCanvas($resizedImage);
    }

}