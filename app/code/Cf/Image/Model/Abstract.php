<?php
/**
 * Codefathers Magento Image Module
 *
 * @category    Cf
 * @package     Cf_Image
 * @copyright   Copyright (c) Achim Schweisgut, codefathers 2016
 */



/**
 * Class Cf_Image_Model_Abstract
 */
abstract class Cf_Image_Model_Abstract extends Varien_Object
{
    const PORTRAIT_RATIO = 4/5;

    

    /**
     * 
     */
    public function __construct()
    {
        // to override
    }


    
    /**
     * 
     * @return boolean
     */
    public function exists()
    {
        if (!$this->getPath()) {
            return false;
        }
        $file = trim((string) $this->getFile());
        $result = ($file && 
                strlen($file) && 
                file_exists($file) && 
                is_readable($file)) ? true : false;
        return $result;
    }
    
    /**
     * 
     * @return int
     */
    public function getWidth()
    {
        if ($file = $this->getFile()) {
            try {
                return (int) Cf_Image_Model_Adapter_Abstract::getImageWidth($file);
            } catch (Exception $e) {
                // do nothing
            }
        }
        return 0;
    }

   

    /**
     * 
     * @return integer
     */
    public function getHeight()
    {
        if ($file = $this->getFile()) {
            try {
                $result = Cf_Image_Model_Adapter_Abstract::getImageHeight($file);
                return $result;
            } catch (Exception $e) {
                // do nothing
            }
        }
        return 0;
    }

  
    
    
    /**
     * 
     * returns the store for this image
     * 
     * @return Mage_Core_Model_Store
     */
    public function getStore()
    {
        if ($this->hasStore()) {
            return $this->getData('store');
        }
        return Mage::app()->getStore();
    }


    /**
     * returns the absolute file path + name 
     * @return string
     * 
     */
    abstract public function getFile();
    
    
    /**
     * 
     * 
     * @param string $result
     * @return string
     * 
     */
    protected function _removeTrailingSlashes($result)
    {
        while (strlen($result) && (($lastChar = $result[strlen($result)-1]) && ($lastChar == '/' || $lastChar == '\\'))) {
            $result = substr($result, 0, strlen($result)-1);
        }
        return $result;
    }
    
    /**
     * 
     * 
     * @param string $result
     * @return string
     * 
     */
    protected function _removeLeadingSlashes($result)
    {
        while (strlen($result) && ($result[0] == '/' || $result[0] == '\\')) {
            $result = substr($result, 1);
        }
        return $result;
    }


    /**
     * @param $width
     * @param float|int $ratio
     * @return string
     */
    public function getPortraitUrl($width, $ratio = self::PORTRAIT_RATIO)
    {
        return $this->getUrlByRatio($width, $ratio);
    }

    /**
     * @param $width
     * @param float|int $ratio
     * @return string
     */
    public function getUrlByRatio($width, $ratio)
    {
        return $this->getUrl($width, $width*(1/$ratio));
    }
    
    /**
     * 
     * @param integer $width
     * @param integer $height
     * @return string
     * @throws Exception
     */
    final public function getUrl($width = 0, $height = 0)
    {
        try {
            $result = $this->_getUrl($width, $height);
        } catch (Exception $e) {
            Mage::log($e->getMessage(), Zend_Log::CRIT, 'system.log');
            $result = Mage::getDesign()->getSkinUrl('images/default.png');
        }
        return $result;
    }
    
    
    /**
     * @return string
     */
    abstract protected function _getUrl($width, $height);
    
    
    
  
}