<?php
/**
 * Codefathers Magento Image Module
 *
 * @category    Cf
 * @package     Cf_Image
 * @copyright   Copyright (c) Achim Schweisgut, codefathers 2016
 */
/**
 * Class Cf_Image_Model_Media
 */
class Cf_Image_Model_Default extends Cf_Image_Model_Media
{


    /**
     * @return string
     */
    public function getPathPrefix()
    {
        return 'catalog/product/placeholder';
    }

    /**
     * @return string
     */
    public function getPath()
    {
        $path = (string) Mage::getStoreConfig('catalog/placeholder/image_placeholder', $this->getStore());
        return $path;
    }

    /**
     *
     * @param integer $width
     * @param integer $height
     * @return string
     * @throws Exception
     */
    protected function _getUrl($width, $height)
    {
        if (!$this->exists()) {
            return Mage::getDesign()->getSkinUrl('images/default.png');
        }
        return parent::_getUrl($width, $height);
    }




}