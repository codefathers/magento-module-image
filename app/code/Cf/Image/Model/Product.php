<?php
/**
 * Codefathers Magento Image Module
 *
 * @category    Cf
 * @package     Cf_Image
 * @copyright   Copyright (c) Achim Schweisgut, codefathers 2016
 */
/**
 * Class Cf_Image_Model_Product
 */
class Cf_Image_Model_Product extends Cf_Image_Model_Media
{

    /**
     * @return string
     */
    public function getPathPrefix()
    {
        return 'catalog/product';
    }






}