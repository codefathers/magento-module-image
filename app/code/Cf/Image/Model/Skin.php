<?php
/**
 * Codefathers Magento Image Module
 *
 * @category    Cf
 * @package     Cf_Image
 * @copyright   Copyright (c) Achim Schweisgut, codefathers 2016
 */

/**
 * Class Cf_Image_Model_Image_Skin
 */
class Cf_Image_Model_Image_Skin extends Cf_Image_Model_Abstract
{

    /**
     * returns the absolute filename 
     * @return string
     * 
     */
    public function getFile()
    {
        //$path = $this->getPath();
        //$result = Mage::getDesign()->getFilename($file, $params);
        return '';
    }
    
    /**
     * 
     * @return string
     */
    protected function _getUrl($width, $height)
    {
        $path = $this->_removeLeadingSlashes($this->getPath());
        $result = Mage::getDesign()->getSkinUrl($path);        
        return $result;
    }    
   
}